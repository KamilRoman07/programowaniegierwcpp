extends KinematicBody2D

#export var bullet_scene: PackedScene
export (int) var detect_radius
export (PackedScene) var PlayerBullet
export (float) var fire_rate
var vis_color = Color(.867, .91, .247, 0.1)
var laser_color = Color(1.0, .329, .298)

#var target
var hit_pos
var can_shoot = true

var speed = 300

var score: int
var timer: float


func _ready():
	$Sprite.self_modulate = Color(0.2, 0, 0)
	#var shape = CircleShape2D.new()
	#shape.radius = detect_radius
	#$Visibility/CollisionShape2D.shape = shape
	$ShootTimer.wait_time = fire_rate

func get_move():
	#sterowanie graczem
	var move: Vector2
	if (Input.is_key_pressed(KEY_D) || Input.is_key_pressed(KEY_RIGHT)):
		move.x += 1
	if (Input.is_key_pressed(KEY_A) || Input.is_key_pressed(KEY_LEFT)):
		move.x -= 1
	if (Input.is_key_pressed(KEY_S) || Input.is_key_pressed(KEY_DOWN)):
		move.y += 1
	if (Input.is_key_pressed(KEY_W) || Input.is_key_pressed(KEY_UP)):
		move.y -= 1
	return move

func get_action():
	var action: Vector2
	if (Input.is_key_pressed(KEY_E) || Input.is_mouse_button_pressed(BUTTON_LEFT)):
		action.x = 1
	if (Input.is_key_pressed(KEY_Q) || Input.is_mouse_button_pressed(BUTTON_RIGHT)):
		action.y = 1
	return action
	
func aim():
	# Strzelamy przed siebie więc kąt taki jaki ma gracz
	var pos = $Sprite.get_local_mouse_position()
	#var pos = get_global_mouse_position()
	if can_shoot:
		shoot(pos)


func shoot(pos):
	var b = PlayerBullet.instance()
	# var a = (pos - global_position).angle()
	var a = get_local_mouse_position().angle()
	b.start(global_position, a)
	get_parent().add_child(b)
	can_shoot = false
	$ShootTimer.start()

func _physics_process(delta):
	update()
	
	#if target:
	var action = Vector2(0,0)
	action = get_action()
	if (action.x == 1):
		aim()
	
	$Sprite.rotation = get_local_mouse_position().angle()
	
	var move = Vector2(0,0)
	move = get_move()
	
	#move_and_slide(move.normalized() * speed)
	move_and_slide(move.rotated(get_local_mouse_position().angle() + PI/2) * speed)
	
		
	#if (action.x == 1):
	#var bullet = PlayerBullet.instance()
	
	#bullet.direction = Vector2(randf() - 0.5, randf() - 0.5).normalized()
	#	get_parent().add_child(bullet)
	#	bullet.global_position = global_position
	
	#if $Sprite/RayCast2D.is_colliding():
	#	var collided = $Sprite/RayCast2D.get_collider()
	#	if collided.is_in_group("enemies"):
	#		collided.damage()

func _on_ShootTimer_timeout():
	can_shoot = true
