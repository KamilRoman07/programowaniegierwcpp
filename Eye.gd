extends Node2D

var Artifact_name = "Eye of the C'thunus - double regeneration"
var global_data

func _ready():
	$Hover.play("Hover")


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		var global_player_data = $"/root/GlobalPlayerData"
		global_player_data.Mana_regen += 5
		global_player_data.HP_regen += 2
		global_data = $"/root/GlobalData"
		global_data.perks_spawned-=1
		$".".visible = false
		global_data = $"/root/GlobalData"
		global_data.ItemDesc.text= Artifact_name
		global_data.ItemDesc.visible = true
		var timer = Timer.new()
		timer.connect("timeout",self,"_on_timer_timeout")
		timer.set_wait_time(5.0) 
		add_child(timer)
		timer.start()

func _on_timer_timeout():
	global_data.ItemDesc.visible = false
	queue_free()
