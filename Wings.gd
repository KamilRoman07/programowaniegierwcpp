extends Node2D


var Artifact_name = "Wings of Yokk' Saroth - increase movement speed"
var global_data

func _ready():
	$HoverPlayer.play("Hover")


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		body.speed += 100
		global_data = $"/root/GlobalData"
		global_data.perks_spawned-=1
		$".".visible = false
		global_data = $"/root/GlobalData"
		global_data.ItemDesc.text= Artifact_name
		global_data.ItemDesc.visible = true
		var timer = Timer.new()
		timer.connect("timeout",self,"_on_timer_timeout")
		timer.set_wait_time(5.0) 
		add_child(timer)
		timer.start()

func _on_timer_timeout():
	global_data.ItemDesc.visible = false
	queue_free()
