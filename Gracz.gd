extends KinematicBody2D

#export var bullet_scene: PackedScene
export (int) var detect_radius
export (PackedScene) var PlayerBullet
export (float) var fire_rate
var vis_color = Color(.867, .91, .247, 0.1)
var laser_color = Color(1.0, .329, .298)

#var target
var hit_pos
var can_shoot = true

var speed = 300

var score: int
var timer: float

var global_player_data
var global_data
var _regen


func _ready():
	$Sprite.self_modulate = Color(0.2, 0, 0)
	#var shape = CircleShape2D.new()
	#shape.radius = detect_radius
	#$Visibility/CollisionShape2D.shape = shape
	$ShootTimer.wait_time = fire_rate
	global_player_data = $"/root/GlobalPlayerData"
	global_data = $"/root/GlobalData"
	global_data.ItemDesc = $Camera2D/ItemDesc
	_regen = Timer.new()
	add_child(_regen)

	_regen.connect("timeout", self, "_on_Timer_timeout")
	_regen.set_wait_time(1.0)
	_regen.set_one_shot(false) # Make sure it loops
	_regen.start()	

func _on_Timer_timeout():
	if global_player_data.MP < global_player_data.Max_MP:
		global_player_data.MP += global_player_data.Mana_regen
	if global_player_data.HP < global_player_data.Max_HP:
		global_player_data.HP += global_player_data.HP_regen
	if global_player_data.HP <= 0:
		get_tree().change_scene("res://GameOver.tscn")
		queue_free()

func get_move():
	#sterowanie graczem
	var move: Vector2
	if (Input.is_key_pressed(KEY_D) || Input.is_key_pressed(KEY_RIGHT)):
		move.x += 1
	if (Input.is_key_pressed(KEY_A) || Input.is_key_pressed(KEY_LEFT)):
		move.x -= 1
	if (Input.is_key_pressed(KEY_S) || Input.is_key_pressed(KEY_DOWN)):
		move.y += 1
	if (Input.is_key_pressed(KEY_W) || Input.is_key_pressed(KEY_UP)):
		move.y -= 1
	return move

func _input(event):
	if event is InputEventKey:
		if event.pressed:
			if global_data.perks_spawned >= 4:
				$Sprite/AnimationPlayer.play("Move_s1")
			if global_data.perks_spawned == 3:
				$Sprite/AnimationPlayer.play("Move_s2")
			if global_data.perks_spawned == 2:
				$Sprite/AnimationPlayer.play("Move_s3")
			if global_data.perks_spawned == 1:
				$Sprite/AnimationPlayer.play("Move_s4")

func get_action():
	var action: Vector2
	if (Input.is_key_pressed(KEY_E) || Input.is_mouse_button_pressed(BUTTON_LEFT)):
		action.x = 1
	if (Input.is_key_pressed(KEY_Q) || Input.is_mouse_button_pressed(BUTTON_RIGHT)):
		action.y = 1
	return action
	
func aim():
	# Strzelamy przed siebie więc kąt taki jaki ma gracz
	var pos = $Sprite.get_local_mouse_position()
	#var pos = get_global_mouse_position()
	if can_shoot:
		shoot(pos)


func shoot(pos):
	if global_player_data.MP >= 30:
		var b = PlayerBullet.instance()
		# var a = (pos - global_position).angle()
		var a = get_local_mouse_position().angle()
		b.start(global_position, a)
		get_parent().add_child(b)
		can_shoot = false
		global_player_data.MP -= 10
		$ShootTimer.set_wait_time(1.0)
		$ShootTimer.start()
		$Sprite/AnimationPlayer.stop()
		if global_data.perks_spawned >= 4:
			$Sprite/AnimationPlayer.play("Attack_s1")
		if global_data.perks_spawned == 3:
			$Sprite/AnimationPlayer.play("Attack_s2")
		if global_data.perks_spawned == 2:
			$Sprite/AnimationPlayer.play("Attack_s3")
		if global_data.perks_spawned == 1:
			$Sprite/AnimationPlayer.play("Attack_s4")

func _physics_process(delta):
	update()
	$Camera2D/Control/HP.max_value = global_player_data.Max_HP
	$Camera2D/Control/HP.value = global_player_data.HP
	$Camera2D/Control/MP.max_value = global_player_data.Max_MP
	$Camera2D/Control/MP.value = global_player_data.MP
	#if target:
	var action = Vector2(0,0)
	action = get_action()
	if (action.x == 1):
		aim()
	
	$Sprite.rotation = get_local_mouse_position().angle()
	
	var move = Vector2(0,0)
	move = get_move()
	
	#move_and_slide(move.normalized() * speed)
	move_and_slide(move.rotated(get_local_mouse_position().angle() + PI/2) * speed)
	
		
	#if (action.x == 1):
	#var bullet = PlayerBullet.instance()
	
	#bullet.direction = Vector2(randf() - 0.5, randf() - 0.5).normalized()
	#	get_parent().add_child(bullet)
	#	bullet.global_position = global_position
	
	#if $Sprite/RayCast2D.is_colliding():
	#	var collided = $Sprite/RayCast2D.get_collider()
	#	if collided.is_in_group("enemies"):
	#		collided.damage()

func _on_ShootTimer_timeout():
	can_shoot = true


func _on_Visibility_area_shape_entered(area_id, area, area_shape, self_shape):
	print(self.name, " wykrył strzał - area.name: ", area.name)
	if String(area.name) == "@EnemyArrow@" || String(area.name) == "EnemyElectroball":
		global_player_data.HP -= 10
