extends Task

var list
var idx = 0
var num_failures = 0

var TESTS = [
	["the enemy in sight", SUCCEEDED],
	["the enemy within range", SUCCEEDED],
	["you can shoot", SUCCEEDED],
	["do you have to run", SUCCEEDED],
	["can you attack", SUCCEEDED],
	["can you heal yourself", SUCCEEDED]
]

func _ready():
	tree = self
	start()
	status = RUNNING

func child_running():
	status = RUNNING

func child_success():
	status = SUCCEEDED
	use_result()

func child_fail():
	status = FAILED
	use_result()

func _process(_delta):
	if status == RUNNING and idx < TESTS.size():
		get_child(idx).run()
		

func use_result():
	if status != 
