extends KinematicBody2D

export (int) var detect_radius
export (float) var fire_rate
#export (PackedScene) var EnemyAttack
var vis_color = Color(.867, .91, .247, 0.1)
var laser_color = Color(1.0, .329, .298)
var sees_the_player = false
var senses_the_player = false
var start_position
var last_target_position = Vector2(0,0)
var enemy_was_spotted = false
var kat
var target
var wall
var hit_pos
var can_shoot = true
var any_projectile
var return_to_start = false
onready var Ray = get_node("WallDetector")
onready var Collider = get_node("CollisionShape2D")
onready var WallCollisionDetector = get_node("WallCollisionDetector")
onready var Detector = WallCollisionDetector.get_node("CollisionShape2D")
onready var nav_2D : Navigation2D = get_parent().get_parent().get_node("Navigation2D")
onready var line_2D : Line2D = get_parent().get_parent().get_node("Line2D")
onready var character : KinematicBody2D = get_parent().get_parent().get_node("Enemies").get_node(self.name)
onready var animation : AnimatedSprite = get_node("Sprite")
var animationSequence = "go"
var hp = 200
var global_player_data

var path : = PoolVector2Array() setget set_path

func move_along_path(distance : float) -> void:
	var start_point : = position
	for i in range (path.size()):
		var distance_to_next : = start_point.distance_to(path[0])
		if distance <= distance_to_next and distance >= 0.0:
			position = start_point.linear_interpolate(path[0], distance / distance_to_next)
			break
		elif distance < 0.0:
			position = path[0]
			#set_proces(false)
			break
		distance -= distance_to_next
		start_point = path[0]
		path.remove(0)


func set_path(value : PoolVector2Array) -> void:
	path = value
	if value.size() == 0:
		return
	
func _ready():
	$Sprite.self_modulate = Color(0.2, 0.2, 0.2)
	var shape = CircleShape2D.new()
	shape.radius = detect_radius
	$Visibility/CollisionShape2D.shape = shape
	$ShootTimer.wait_time = fire_rate
	start_position = global_position
	global_player_data = $"/root/GlobalPlayerData"
	

func _physics_process(delta):
	if hp <= 0:
		queue_free()
	update()
	if target:
		if is_the_enemy_visible(target):
			last_target_position = target.global_position
			enemy_was_spotted = true
			go_to_point(last_target_position, 3)
			aim()
		elif enemy_was_spotted:
			go_to_point(last_target_position, 3)
			if any_projectile:
				turn_to_point(any_projectile.global_position)
				any_projectile = false
			if (global_position - last_target_position).length() < 3:
				enemy_was_spotted = false
				return_to_start = true
				return_to_start_position(start_position)
			any_projectile = false
		elif any_projectile:
			turn_to_point(any_projectile.global_position)
			any_projectile = false
		elif return_to_start == true:
			if (global_position - start_position).length() < 2:
				return_to_start = false
			else:
				return_to_start_position(start_position)
	elif enemy_was_spotted:
		go_to_point(last_target_position, 3)
		if any_projectile:
				turn_to_point(any_projectile.global_position)
				any_projectile = false
		if (global_position - last_target_position).length() < 3:
			enemy_was_spotted = false
			return_to_start = true
			return_to_start_position(start_position)
	elif any_projectile:
		turn_to_point(any_projectile.global_position)
		any_projectile = false
	elif return_to_start == true:
		if (global_position - start_position).length() < 2:
			return_to_start = false
		else:
			return_to_start_position(start_position)



func turn_to_point(point_position):
	rotation = (point_position - global_position).angle() - PI/2


func return_to_start_position(start_position):
		#var new_path: = nav_2D.get_simple_path(global_position, start_position)
		#line_2D.points = new_path
		#character.path = new_path
		go_to_point(start_position, 2)

func go_to_point(point_position, dystans):
	anim("go")
	var new_path: = nav_2D.get_simple_path(global_position, point_position)
	line_2D.points = new_path
	character.path = new_path
	rotation = (point_position - global_position).angle() - PI/2
	move_along_path(dystans)


func is_the_enemy_visible(target) -> bool:
	kat = abs(fmod(2 * PI + fmod(rotation + PI/2, 2 * PI), 2 * PI) - fmod(2 * PI + (target.position - position).angle(), 2 * PI))
	if kat > PI: kat = 2* PI - kat
	var from_Player_to_Enemy: Vector2 = position - target.position
	var to_the_side = Vector2(-from_Player_to_Enemy.y, from_Player_to_Enemy.x).normalized()
	var space_state = get_world_2d().direct_space_state
	var target_extents = target.get_node('CollisionShape2D').shape.radius
	var tl = target.position - to_the_side * target_extents
	var tr = target.position + to_the_side * target_extents
	for pos in [target.position, tl, tr]:
		var test = space_state.intersect_ray(position, pos, [self], collision_mask)
		if (kat < 3*PI/4) && test && test.collider.name == "Player":
			return true
	return false

func aim():
	hit_pos = []
	var space_state = get_world_2d().direct_space_state
	var target_extents = target.get_node('CollisionShape2D').shape.radius
	var from_Player_to_Enemy: Vector2 = position - target.position
	var to_the_side = Vector2(-from_Player_to_Enemy.y, from_Player_to_Enemy.x).normalized()
	var tl = target.position - to_the_side * target_extents
	var tr = target.position + to_the_side * target_extents
	for pos in [target.position, tl, tr]:
		var result = space_state.intersect_ray(position, pos, [self], collision_mask)
		if result:
			hit_pos.append(result.position)
			if result.collider.name == "Player":
				$Sprite.self_modulate =  Color(1.0, 1.0, 1.0)
				#rotation = (target.position - position).angle() - PI/2
				if can_shoot:
					shoot(pos)
				break


func shoot(pos):
	anim("attack")
	#var b = EnemyAttack.instance()
	#var a = (pos - global_position).angle()
	#b.start(global_position, a + rand_range(-0.05, 0.05))
	#get_parent().add_child(b)
	can_shoot = false
	$ShootTimer.start()

func anim(sequence):
	if can_shoot == true:
		#if sequence != animationSequence:
		animation.animation = sequence

func _draw():
	draw_circle(Vector2(), detect_radius, vis_color)
	if target:
		if enemy_was_spotted:
			for hit in hit_pos:
				draw_circle((hit - position).rotated(-rotation), 5, laser_color)
				draw_line(Vector2(), (hit - position).rotated(-rotation), laser_color)


func _on_Visibility_body_entered(body):
	if target:
		return
	target = body
	print("W zasięgu wzroku pojawił się - body.name: ", body.name)


func _on_Visibility_body_exited(body):
	if body == target:
		target = null
		$Sprite.self_modulate = Color(0.2, 0.2, 0.2)

func _on_ShootTimer_timeout():
	can_shoot = true


func _on_WallCollisionDetector_area_entered(area):
	# wykrywa strzały 
	print(self.name, " wykrył strzał - area.name: ", area.name)
	if any_projectile && area == any_projectile:
		return
	elif String(area.name) == "PlayerBullet":
		any_projectile = area
		hp -= global_player_data.Damage

func _on_WallCollisionDetector_body_shape_entered(body_id, body, body_shape, area_shape):
	#print("_on_WallCollisionDetector_body_shape_entered(body_id, body, body_shape, area_shape): ", self.name, "    body_id: ", body_id, "    body: ", String(body.name), "    body_shape: ", body_shape, "    area_shape: ",area_shape)
	if String(body.name) == "Player":
		enemy_was_spotted = true
		print(self.name, " zderzył się z - body.name: ", body.name )
	elif String(body.name) != "TileMap":
		print(self.name, " zderzył się z - body.name: ", body.name )
		return
	elif wall && body == wall:
		return
	elif String(body.name) == "TileMap":
		print(self.name, " zderzył się z - body.name: ", body.name )
		wall = body
		print(self.name, "Body.name: ", body.name)


func _on_WallCollisionDetector_body_shape_exited(body_id, body, body_shape, area_shape):
	if body == wall:
		wall = null


func _on_Visibility_body_shape_entered(body_id, body, body_shape, area_shape):
	print(self.name, " zobaczył strzał - body.name: ", body.name )
	if String(body.name) == "PlayerBullet":
		any_projectile = body
		print(self.name, " zobaczył strzał - body.name: ", body.name )

