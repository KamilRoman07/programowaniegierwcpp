extends KinematicBody2D

export (int) var detect_radius
export (float) var fire_rate
export (PackedScene) var EnemyElectroball
var vis_color = Color(.867, .91, .247, 0.1)
var laser_color = Color(1.0, .329, .298)
var sees_the_player = false
var senses_the_player = false
var start_position = global_position
var kierunek = 1

var target
var hit_pos
var can_shoot = true

func _ready():
	$Sprite.self_modulate = Color(0.2, 0.2, 0.2)
	var shape = CircleShape2D.new()
	shape.radius = detect_radius
	$Visibility/CollisionShape2D.shape = shape
	$ShootTimer.wait_time = fire_rate

func _physics_process(delta):
	update()
	#if ((global_position - start_position).length() > Vector2(12,12).length()):
	#	kierunek = -1
	#elif ((start_position - global_position).length() > Vector2(12,12).length()):
	#	kierunek = 1
	
	if target:
		# Warunki widoczności targetu:
		# 1. sprawdzamy czy w polu widzenia
		# 2. sprawdzamy czy nie jest za zasłoną
		
		
		var move = (target.position - position).normalized()
		move_and_slide(move.rotated(position.angle_to(target.position)) * 100)
		aim()

func aim():
	hit_pos = []
	var space_state = get_world_2d().direct_space_state
	var target_extents = target.get_node('CollisionShape2D').shape.radius
	var from_Player_to_Enemy: Vector2 = position - target.position
	var to_the_side = Vector2(-from_Player_to_Enemy.y, from_Player_to_Enemy.x).normalized()
	var tl = target.position - to_the_side * target_extents
	var tr = target.position + to_the_side * target_extents
	for pos in [target.position, tl, tr]:
		var result = space_state.intersect_ray(position, pos, [self], collision_mask)
		if result:
			hit_pos.append(result.position)
			if result.collider.name == "Player":
				$Sprite.self_modulate =  Color(1.0, 1.0, 1.0)
				rotation = (target.position - position).angle() - PI/2
				if can_shoot:
					shoot(pos)
				break

func shoot(pos):
	var b = EnemyElectroball.instance()
	var a = (pos - global_position).angle()
	b.start(global_position, a + rand_range(-0.05, 0.05))
	get_parent().add_child(b)
	can_shoot = false
	$ShootTimer.start()

func _draw():
	draw_circle(Vector2(), detect_radius, vis_color)
	if target:
		for hit in hit_pos:
			draw_circle((hit - position).rotated(-rotation), 5, laser_color)
			draw_line(Vector2(), (hit - position).rotated(-rotation), laser_color)


func _on_Visibility_body_entered(body):
	if target:
		return
	target = body

func _on_Visibility_body_exited(body):
	if body == target:
		target = null
		$Sprite.self_modulate = Color(0.2, 0.2, 0.2)

func _on_ShootTimer_timeout():
	can_shoot = true

