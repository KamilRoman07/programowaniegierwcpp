extends Node2D

var Artifact_name = "Hearth of Darkness - increase Max HP by 100"
var global_data

func _ready():
	$HoverPlayer.play("Hover") 


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		var global_player_data = $"/root/GlobalPlayerData"
		global_player_data.Max_HP += 100
		global_player_data.HP = global_player_data.Max_HP
		global_data = $"/root/GlobalData"
		global_data.perks_spawned-=1
		$".".visible = false
		global_data = $"/root/GlobalData"
		global_data.ItemDesc.text= Artifact_name
		global_data.ItemDesc.visible = true
		var timer = Timer.new()
		timer.connect("timeout",self,"_on_timer_timeout")
		timer.set_wait_time(5.0) 
		add_child(timer)
		timer.start()

func _on_timer_timeout():
	global_data.ItemDesc.visible = false
	queue_free()
