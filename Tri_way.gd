extends Node2D

var directions
var global_data

func _ready():
	global_data = $"/root/GlobalData"

func _create():
	randomize()
	var rand = randi()%4+1
	if rand == 1:
		directions = ["Up","Down", "Left"]
	elif rand == 2:
		directions = ["Up","Right", "Left"]
		set_rotation_degrees(90)
	elif rand == 3:
		directions = ["Up","Right", "Down"]
		set_rotation_degrees(180)
	else:
		directions = ["Right","Down", "Left"]
		set_rotation_degrees(270)
	_rotateEnemyPositions(rand)

func _rotateEnemyPositions(rand):
	var radians = ((rand - 1)*90) * PI/180
	for enemy_spawn_point in $EnemySpawnPoints.get_children():
		var old_x = enemy_spawn_point.position.x
		var old_y = (-1) * enemy_spawn_point.position.y
		enemy_spawn_point.position.x = cos(radians) * old_x + sin(radians) * old_y 
		enemy_spawn_point.position.y = ((-1) * sin(radians) * old_x + cos(radians) * old_y) * (-1)
	for artifact_spawn_point in $PerkSpawns.get_children():
		var old_x = artifact_spawn_point.position.x
		var old_y = (-1) * artifact_spawn_point.position.y
		artifact_spawn_point.position.x = cos(radians) * old_x + sin(radians) * old_y 
		artifact_spawn_point.position.y = ((-1) * sin(radians) * old_x + cos(radians) * old_y) * (-1)

func _spawnEnemies():
	for enemy_spawn_point in $EnemySpawnPoints.get_children():
		if randi()%2 + 1 == 1:
			var enemy
			var enemy_type = randi()%4 + 1
			if enemy_type == 1:
				enemy = load("Mag.tscn").instance()
			elif enemy_type == 2:
				enemy = load("Knight.tscn").instance()
			elif enemy_type == 3:
				enemy = load("Archer.tscn").instance()
			else:
				enemy = load("Executioner.tscn").instance()
			enemy.set_position(enemy_spawn_point.position)
			$Enemies.add_child(enemy)

func _spawnArtifact():
	if randi()%3 < 3:
		if global_data.perk_array.size() > 0:
			var perk_number = randi()% global_data.perk_array.size()
			var perk_scene_name = global_data.perk_array[perk_number] + ".tscn"
			var perk = load(perk_scene_name).instance()
			var perk_position_number = randi()% $PerkSpawns.get_children().size()
			perk.set_position($PerkSpawns.get_children()[perk_position_number].position)
			add_child(perk)
			global_data.perks_spawned += 1
			global_data.perk_array.remove(perk_number)

func _updateEnemySpawnPositions(vector):
	vector = Vector2(vector);
	for enemy_spawn_point in $EnemySpawnPoints.get_children():
		enemy_spawn_point.position.x = enemy_spawn_point.position.x + vector.x
		enemy_spawn_point.position.y = enemy_spawn_point.position.y + vector.y
