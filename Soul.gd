extends Node2D

var Artifact_name = "Darkened soul - Increases max MP by 50"
var global_data

func _ready():
	$HoverPlayer.play("Hover")


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		$".".visible = false
		var global_player_data = $"/root/GlobalPlayerData"
		global_player_data.Max_MP += 50
		global_player_data.MP = global_player_data.Max_MP
		
		global_data = $"/root/GlobalData"
		global_data.ItemDesc.text= Artifact_name
		global_data.ItemDesc.visible = true
		var timer = Timer.new()
		timer.connect("timeout",self,"_on_timer_timeout")
		timer.set_wait_time(5.0) 
		add_child(timer)
		timer.start()
		global_data.perks_spawned-=1

func _on_timer_timeout():
	global_data.ItemDesc.visible = false
	queue_free()
