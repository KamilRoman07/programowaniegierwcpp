extends Node2D

var ways_out = ["Up","Down","Left","Right"]
var root
var y_position
var x_position
var level_array = [[0,0,0,0,0],
[0,0,0,0,0],
[0,0,-1,0,0],
[0,0,0,0,0],
[0,0,0,0,0]]
var enemy_spawn_points
var global_data

func _ready():
	randomize()
	enemy_spawn_points = $EnemySpawnPoints.get_children()
	global_data = $"/root/GlobalData"
	_spawnPerk()
	_randomizeWorldArray()
	_buildWolrd() 

func _spawnPerk():
	var perk_number = randi()% global_data.perk_array.size()
	var perk
	if perk_number == 0: 
		perk = load("Eye.tscn").instance()
	elif perk_number == 1:
		perk = load("Wings.tscn").instance()
	elif perk_number == 2:
		perk = load("Flesh.tscn").instance()
	elif perk_number == 3:
		perk = load("Soul.tscn").instance()
	else:
		perk = load("Tentacles.tscn").instance()
	perk.set_position($PerkSpawnPoint.position)
	global_data.perk_array.remove(perk_number)
	add_child(perk)

func _randomizeWorldArray():
	randomize()
	var last_position = Vector2(2,2)
	var room_amount = randi()%5 + 5
	var last_direction
	var direction
	for i in range(room_amount):
		var loop = false
		var try_lock = 0
		while loop == false && try_lock < 50:
			direction = randi()%4 + 1
			var x = last_position.x
			var y = last_position.y
			if direction == 1 && last_direction!= 2:
				if y - 1 >= 0 && level_array[y-1][x] == 0:
					var room = randi()%2 + 1
					last_position = Vector2(last_position.x,last_position.y-1)
					level_array[last_position.y][last_position.x] = room
					loop = true
					last_direction = direction
			elif direction == 2 && last_direction!= 1:
				if y + 1 < 5 && level_array[y+1][x] == 0:
					var room = randi()%2 + 1
					last_position = Vector2(last_position.x,last_position.y+1)
					level_array[last_position.y][last_position.x] = room
					loop = true
					last_direction = direction
			elif direction == 3 && last_direction!= 4:
				if x - 1 >= 0 && level_array[y][x-1] == 0:
					var room = randi()%2 + 1
					last_position = Vector2(last_position.x-1,last_position.y)
					level_array[last_position.y][last_position.x] = room
					loop = true
					last_direction = direction
			elif direction == 4 && last_direction != 3:
				if x + 1 < 5  && level_array[y][x+1] == 0:
					var room = randi()%2 + 1
					last_position = Vector2(last_position.x+1,last_position.y)
					level_array[last_position.y][last_position.x] = room
					loop = true
					last_direction = direction
			try_lock += 1
		if try_lock >= 50:
			print("Locked")


func _buildWolrd():
	for i in range(5):
		for j in range (5):
			if level_array[i][j] == 1:
				var crossroads = load("Room2.tscn").instance()
				crossroads.set_position(Vector2(-6700 + 3350 * j,-6700 + 3350 * i))
				crossroads._updateEnemySpawnPositions(crossroads.position)
				crossroads._spawnEnemies()
				$Rooms.add_child(crossroads)
				crossroads._spawnArtifact()
			elif level_array[i][j] == 2:
				var tri_way = load("Tri_way.tscn").instance()
				tri_way._create()
				tri_way.set_position(Vector2(-6700 + 3350 * j,-6700 + 3350 * i))
				tri_way._updateEnemySpawnPositions(tri_way.position)
				tri_way._spawnEnemies()
				$Rooms.add_child(tri_way)
				tri_way._spawnArtifact()
			elif level_array[i][j] == 0:
				var blank = load("Blank.tscn").instance()
				blank.set_position(Vector2(-6700 + 3350 * j,-6700 + 3350 * i))
				$Rooms.add_child(blank)
	for enemy_spawn_point in enemy_spawn_points:
		if randi()%2 + 1 == 1:
			var enemy
			var enemy_type = randi()%4 + 1
			if enemy_type == 1:
				enemy = load("Mag.tscn").instance()
			elif enemy_type == 2:
				enemy = load("Knight.tscn").instance()
			elif enemy_type == 3:
				enemy = load("Archer.tscn").instance()
			else:
				enemy = load("Executioner.tscn").instance()
			enemy.set_position(enemy_spawn_point.position)
			$Enemies.add_child(enemy)

func _process(delta):
	if global_data.perks_spawned == 0:
		queue_free()
