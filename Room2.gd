extends Node2D

var ways_out = ["Up","Down","Left","Right"]

var global_data

func _ready():
	global_data = $"/root/GlobalData"
	randomize()

func _spawnEnemies():
	for enemy_spawn_point in $EnemySpawnPoints.get_children():
		if randi()%2 + 1 == 1:
			var enemy
			var enemy_type = randi()%4 + 1
			if enemy_type == 1:
				enemy = load("Mag.tscn").instance()
			elif enemy_type == 2:
				enemy = load("Knight.tscn").instance()
			elif enemy_type == 3:
				enemy = load("Archer.tscn").instance()
			else:
				enemy = load("Executioner.tscn").instance()
			enemy.set_position(enemy_spawn_point.position)
			$Enemies.add_child(enemy)

func _spawnArtifact():
	var global_data = $"/root/GlobalData"
	if randi()%3 < 3:
		if global_data.perk_array.size() > 0:
			var perk_number = randi()% global_data.perk_array.size()
			var perk_scene_name = global_data.perk_array[perk_number] + ".tscn"
			var perk = load(perk_scene_name).instance()
			var perk_position_number = randi()% $PerkSpawns.get_children().size()
			perk.set_position($PerkSpawns.get_children()[perk_position_number].position)
			add_child(perk)
			global_data.perks_spawned += 1
			global_data.perk_array.remove(perk_number)

func _updateEnemySpawnPositions(vector):
	vector = Vector2(vector);
	for enemy_spawn_point in $EnemySpawnPoints.get_children():
		enemy_spawn_point.position.x = enemy_spawn_point.position.x + vector.x
		enemy_spawn_point.position.y = enemy_spawn_point.position.y + vector.y
	for artifact_spawn_point in $PerkSpawns.get_children():
		artifact_spawn_point.position.x = artifact_spawn_point.position.x + vector.x
		artifact_spawn_point.position.y = artifact_spawn_point.position.y + vector.y
