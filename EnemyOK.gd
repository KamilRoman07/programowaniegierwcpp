extends KinematicBody2D


var positionStart = position.x
var direction = 1

var hp = 60

func damage():
	hp -= 1
	modulate.a = (hp / 10.0)
	
	if hp == 0:
		queue_free()


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.

func _ready():
	position = Vector2(get_viewport().size.x/2, get_viewport().size.y/2)
	#scale = Vector2(1.5,1.5)
	#rotate(deg2rad(90))

func _process(delta):
	
	if (abs(positionStart - position.x)  > 100):
		direction = -1 * direction

	translate(Vector2( direction * 100 * delta,0))

